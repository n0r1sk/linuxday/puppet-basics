class linuxtag_groups::service_x (
  $testparameter = 'standard'
)  {
  include linuxtag::1_directory

  if $testparameter == 'standard' {
    include linuxtag::2_file
  } else {
    include linuxtag::3_link
  }
}
