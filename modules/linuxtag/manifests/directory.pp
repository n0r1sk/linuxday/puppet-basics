class linuxtag::directory {
  file { '/srv/linuxtag':
    ensure => 'directory',
    owner  => 'root',
		group  => 'root',
		mode   => '0750'
  }
}
