class linuxtag::service_reboot {
  $svc_name = 'mysql'

  package { "${svc_name}-server":
    ensure => 'installed',
  }

  service { $svc_name:
    ensure  => 'running',
    enable  => true,
    require => Package["${svc_name}-server"],
  }

  # add a notify to the file resource
  file { "/etc/${svc_name}/${svc_name}.conf.d/${svc_name}d.cnf":
    notify  => Service["${svc_name}"],  # this sets up the relationship
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
    require => Package["${svc_name}-server"],
    content => template("linuxtag//${svc_name}/${svc_name}d.cnf.erb"),
  }
}
