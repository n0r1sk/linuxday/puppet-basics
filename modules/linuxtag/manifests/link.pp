class linuxtag::link {
  file { '/srv/linuxtag/link-to-hosts':
    ensure => 'link',
    target => '/etc/hosts',
  }
}
