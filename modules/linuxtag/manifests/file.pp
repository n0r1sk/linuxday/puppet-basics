class linuxtag::file {
  file { '/srv/linuxtag/test.conf':
		ensure => present,
		owner  => 'root',
		group  => 'root',
		mode   => '0640',
		source => 'puppet:///modules/linuxtag/test.conf'
	}

  file { '/srv/linuxtag/host.conf':
    ensure => 'present',
    source => [
        "puppet:///modules/linuxtag/host.${::hostname}.conf",
        'puppet:///modules/linuxtag/host.conf'
    ],
  }
}
